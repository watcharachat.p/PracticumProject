package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import java.util.Random;

import org.usb4java.Device;

public class World {
    private QuickAttacker quickAttacker;
    private BlackBlock blackBlock;
    Random rand = new Random();
    int[] numBlock = new int[4];
    int[] numSword = new int[6];
    int[] numDefense = new int[2];
    boolean[] sword;
    boolean[] defense;
    boolean[] player1;
    boolean[] player2; 
    int timePlayer1;
    int timePlayer2;
    int gameStage = 0;/// edited from =1
    int scorePlayer1 = 0;
    int scorePlayer2 = 0;
    
    float delay = 1;
    
    private McuWithPeriBoard peri;
    
    public World(QuickAttacker quickAttacker){
        this.quickAttacker = quickAttacker;
        blackBlock = new BlackBlock(quickAttacker);
        /*
        do {
            numBlock = getRandom(numBlock);
        } while(checkArray(numBlock) == false);
        player1 = setButton(numBlock);
        player2 = setButton(numBlock);
        do {
            do {
                numSword = getRandom(numSword);
            } while(checkArray(numSword) == false);
            do {
                numDefense = getRandom(numDefense);
            } while(checkArray(numDefense) == false);
        } while(checkArray2(numDefense,numSword) == false);
        sword = setButton(numSword);
        defense = setButton(numDefense);
        */
        McuBoard.initUsb();

        try
        {
        	Device[] devices = McuBoard.findBoards();
        	
        	if (devices.length == 0) {
                System.out.format("** Practicum board not found **\n");
                return;
        	}
        	else {
                System.out.format("** Found %d practicum board(s) **\n", devices.length);
        	}
            peri = new McuWithPeriBoard(devices[0]);

            System.out.format("** Practicum board found **\n");
            System.out.format("** Manufacturer: %s\n", peri.getManufacturer());
            System.out.format("** Product: %s\n", peri.getProduct());          
        }
        catch (Exception e)
        {
            System.out.println(e);
        }

        
    }
    
    public void render(){
        if(gameStage==1) {
            int time1 = player1Stage(player1,numBlock);
            int time2 = player2Stage(player2,numBlock);
            if(time1!=0 && time2!=0)
                compare(time1, time2);
        }
        if(gameStage==2) {
            int atime1 = player1Stage(sword,numSword);
            int atime2 = player2Stage(defense,numDefense);
            if(atime1>atime2) {
                System.out.println("Attack Success!!");
                this.scorePlayer1++;
                gameStage=0;
            }
            if(atime1<atime2) {
                System.out.println("Attack fail!!");
                gameStage=0;
            }
        }
        if(gameStage==3) {
            int btime1 = player1Stage(defense,numDefense);
            int btime2 = player2Stage(sword,numSword);
            if(btime1>btime2) {
                System.out.println("Attack fail!!");
                gameStage=0;
            }
            if(btime1<btime2) {
                System.out.println("Attack Success!!");
                this.scorePlayer2++;
                gameStage=0;
            }
        }
        ////////////////added by Tay
        if(gameStage == 0)
        {
            do {
                numBlock = getRandom(numBlock);
            } while(checkArray(numBlock) == false);
            player1 = setButton(numBlock);
            player2 = setButton(numBlock);
            do {
                do {
                    numSword = getRandom(numSword);
                } while(checkArray(numSword) == false);
                do {
                    numDefense = getRandom(numDefense);
                } while(checkArray(numDefense) == false);
            } while(checkArray2(numDefense,numSword) == false);
            sword = setButton(numSword);
            defense = setButton(numDefense);
        	gameStage =1;
        }
        ////////////////
    }
    
    public int player1Stage(boolean[] player1, int[] num){
        if(!checkPass(player1,num)){
            timePlayer1 += 1;
            if(peri.getSwitch1(0,0)){
                player1[0]=true;
            }
            if(peri.getSwitch1(0,1)){
                player1[1]=true;
            } 
            if(peri.getSwitch1(0,2)){
                player1[2]=true;
            } 
            if(peri.getSwitch1(0,3)){
                player1[3]=true;
            }
            if(peri.getSwitch1(1,0)){
                player1[4]=true;
            }
            if(peri.getSwitch1(1,1)){
                player1[5]=true;
            } 
            if(peri.getSwitch1(1,2)){
                player1[6]=true;
            } 
            if(peri.getSwitch1(1,3)){
                player1[7]=true;
            }
            if(peri.getSwitch1(2,0)){
                player1[8]=true;
            }
            if(peri.getSwitch1(2,1)){
                player1[9]=true;
            } 
            if(peri.getSwitch1(2,2)){
                player1[10]=true;
            } 
            if(peri.getSwitch1(2,3)){
                player1[11]=true;
            }
            if(peri.getSwitch1(3,0)){
                player1[12]=true;
            }
            if(peri.getSwitch1(3,1)){
                player1[13]=true;
            } 
            if(peri.getSwitch1(3,2)){
                player1[14]=true;
            } 
            if(peri.getSwitch1(3,3)){
                player1[15]=true;
            }
        }
        else {
            timePlayer1 += 0;
            return timePlayer1;
        }
        return 0;
    }
    
     public int player2Stage(boolean[] player2, int[] num){ 
        if(!checkPass(player2,num)){
            timePlayer2 += 1;
            if(peri.getSwitch2(0,0)){
                player2[0]=true;
            }
            if(peri.getSwitch2(0,1)){
                player2[1]=true;
            } 
            if(peri.getSwitch2(0,2)){
                player2[2]=true;
            } 
            if(peri.getSwitch2(0,3)){
                player2[3]=true;
            }
            if(peri.getSwitch2(1,0)){
                player2[4]=true;
            }
            if(peri.getSwitch2(1,1)){
                player2[5]=true;
            } 
            if(peri.getSwitch2(1,2)){
                player2[6]=true;
            } 
            if(peri.getSwitch2(1,3)){
                player2[7]=true;
            }
            if(peri.getSwitch2(2,0)){
                player2[8]=true;
            }
            if(peri.getSwitch2(2,1)){
                player2[9]=true;
            } 
            if(peri.getSwitch2(2,2)){
                player2[10]=true;
            } 
            if(peri.getSwitch2(2,3)){
                player2[11]=true;
            }
            if(peri.getSwitch2(3,0)){
                player2[12]=true;
            }
            if(peri.getSwitch2(3,1)){
                player2[13]=true;
            } 
            if(peri.getSwitch2(3,2)){
                player2[14]=true;
            } 
            if(peri.getSwitch2(3,3)){
                player2[15]=true;
            }
        }
        else{
            timePlayer2 += 0;
            return timePlayer2;
        }
        return 0;
    }
     
    public void compare(int time1, int time2) {
        if (time1<time2) {
            System.out.println("Player1 Win!!!");
            gameStage = 2;
            System.out.println(gameStage);
        } else if (time2<time1) {
            System.out.println("Player2 Win!!!");
            gameStage = 3;
            System.out.println(gameStage);
        } else if (time1==time2) {
            System.out.println("Draw");
            gameStage = 0;
            System.out.println(gameStage);
        }
        timePlayer1 =0;
        timePlayer2 =0;
    }
    
    public boolean checkPass(boolean[] arr,int[] num){
        int count = 0;
        if(gameStage==1) {
            for(int i=0 ; i<arr.length; i++){
                if(arr[i]==false)
                    return false;
            }
        }
        else {
            for(int i=0 ; i<arr.length; i++){
                if(arr[i]==false) {
                    count++;
                }
            }
            if(count==num.length)
                return false;
            return true;
        }
        return true;
    }
    
    public boolean[] setButton(int[] pos){
        boolean[] bool = new boolean[16];
        for(int j=0; j<bool.length; j++) {
                bool[j] = true;
        }
        for(int i=0; i<pos.length ; i++){
            for(int j=0; j<bool.length; j++) {
                if(pos[i]==j)
                    bool[j] = false;
            }
        }
        return bool;
    }
    
    public int[] getRandom(int[] arr){
        for(int i=0; i<arr.length ; i++){
            arr[i] = rand.nextInt(15)+0;
        }
        return arr;
    }
    
    public boolean checkArray(int[] arr){
        for(int i=0; i<arr.length-1 ; i++){
            for(int j=i+1 ; j<arr.length ; j++){
                if(arr[i] == arr[j]){
                    return false;
                }
            }
        }
        return true;
    }
    
    public boolean checkArray2(int[] arr1,int[] arr2){
        for(int i=0; i<arr1.length ; i++){
            for(int j=0 ; j<arr2.length ; j++){
                if(arr1[i] == arr2[j]){
                    return false;
                }
            }
        }
        return true;
    }
    
    public int[] getPositionXBlock(){
        return blackBlock.getPositionXBlock(numBlock);
    }
    
    public int[] getPositionYBlock(){
        return blackBlock.getPositionYBlock(numBlock);
    }
    
    public int[] getPositionSwordX(){
        return blackBlock.getPositionXBlock(numSword);
    }
    
    public int[] getPositionSwordY(){
        return blackBlock.getPositionYBlock(numSword);
    }
    public int[] getPositionDefenseX(){
        return blackBlock.getPositionXBlock(numDefense);
    }
    
    public int[] getPositionDefenseY(){
        return blackBlock.getPositionYBlock(numDefense);
    }
    
    public boolean[] getPlayer1(){
        return player1;
    }
    
    public boolean[] getPlayer2(){
        return player2;
    }
    
    public int getGameStage() {
        return gameStage;
    }
    
    public int getScorePlayer1() {
        return scorePlayer1;
    }
    
    public int getScorePlayer2() {
        return scorePlayer2;
    }
}
