package com.mygdx.game;

import org.usb4java.Device;

public class McuWithPeriBoard extends McuBoard
{
	private static final byte RQ_SET_LED       = 0;
    private static final byte RQ_SET_LED_VALUE = 1;
    private static final byte RQ_GET_SWITCH    = 2;
    private static final byte RQ_GET_LIGHT     = 3;

    public McuWithPeriBoard(Device device) 
    {
		super(device);
	}
    /**
     * Read and return the light intensity
     * 
     * @return a value between 0-1023; the greater the intensity, the higher the value
     */
    public boolean getSwitch1(int row,int col)
    {
        byte[] ret = this.read((byte)1, (short) row, (short) col);
        return ret[0]==1;
    }
    public boolean getSwitch2(int row,int col)
    {
        byte[] ret = this.read((byte)2, (short) row, (short) col);
        return ret[0]==1;
    }
}
